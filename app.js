const puppeteer = require('puppeteer');

const home_url = 'https://abp-org.web.app/en/index';
let href_selectors = [
  '[href="/en/download"]',
  '[href="/en/about"]',
  '[href="/en/donate"]',
  '[href="/en/adblock-plus-chrome"]',
  '[href="/en/adblock-plus-firefox"]',
  '[href="/en/ad-blocker-safari"]',
  '[href="/en/adblock-plus-opera"]',
  '[href="/en/android-install"]',
  '[href="/en/acceptable-ads"]',
  '[href="/en/documentation"]',
  '[href="/en/deployments"]',
  '[href="/en/bugs"]',
  '[href="/en/faq-privacy"]',
  '[href="/en/contribute"]',
  '[href="/en/tools"]',
  '[href="/en/partner-integrations"]',
  '[href="/en/terms"]',
  '[href="/en/privacy"]',
  '[href="/en/imprint"]'
];

// Return random number between 1000 and 10000
const rand_time = () => {
  return (Math.floor(Math.random() * 10) + 1) * 1000
};

// Return shuffled array (Using the Fisher-Yates shuffle)
const shuffle = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array
};

(async () => {
  const browser = await puppeteer.launch();
  const page =  await browser.newPage();
  await page.setViewport({ width: 1920, height: 1080 });
  await page.goto(home_url);
  console.log('Navigated to ' + (await page.title()) + ' at ' +  page.url() +'\n');

  shuffled_selectors = shuffle(href_selectors);
  for (const [index, href_selector] of shuffled_selectors.entries()) {
    const wait_time = rand_time();
    console.log('Waiting for ' + wait_time/1000 + 's');
    await page.waitForTimeout(wait_time);
    await Promise.all([
      page.waitForNavigation(),
      page.click(href_selector),
    ]);
    console.log('Navigated to ' + (await page.title()) + ' at ' +  page.url() +'\n');
  }

  await browser.close();
})();
