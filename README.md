# pupsketeer

Randomly click through abp.o

NOTE: Currently only works on macOS

## Usage

```bash
npm install
npm start
```

## Todo

- Handle errors + exceptions + rejected promises
- Allow for clicking through sites other than abp.o
- Address failure to run on Debian + Ubuntu
